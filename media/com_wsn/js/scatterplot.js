var chart = document.getElementsByTagName('scatterplot')[0];
var datafile = chart.getAttribute('src');
var yunit =  chart.getAttribute('yunit');
var sensorType =  chart.getAttribute('sensorType');
var sensorId =  chart.getAttribute('sensorId');
var numPoints =  chart.getAttribute('numPoints');

var margin = {top: 20, right: 80, bottom: 30, left: 5},
    width = 900 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

var x = d3.time.scale()
    .range([0, width]);
var y = d3.scale.linear()
    .range([height, 0]);

var color = d3.scale.category10();

var dateFormatter = d3.time.format("%m-%d %H:%M");
var xAxis = d3.svg.axis()
    .scale(x)
    .tickFormat(function(d) { return dateFormatter(d); })
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

var svg = d3.select("scatterplot").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

var formatDate = d3.time.format("%Y-%m-%d %H:%M:%S"),
parseDate = formatDate.parse,
bisectDate = d3.bisector(function(d) { return d.ts; }).left,
formatOutput0 = function(d) { return formatDate(d.ts) + ": " + d.val + " " + yunit; };

d3.csv(datafile, function(error, data) {
  data.forEach(function(d) {
	  d.ts = parseDate(d.ts);
	  d.val = +d.val;
  });

  x.domain([data[0].ts, data[data.length - 1].ts]);
  y.domain(d3.extent(data, function(d) { return d.val; }));

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
    .append("text")
      .attr("class", "label")
      .attr("x", width)
      .attr("y", -6)
      .style("text-anchor", "end")
      .text("");

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("class", "label")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text(yunit);

  svg.selectAll(".dot")
      .data(data)
    .enter().append("circle")
      .attr("class", "dot")
      .attr("r", 3.5)
      .attr("cx", function(d) { return x(d.ts); })
      .attr("cy", function(d) { return y(d.val)-100; })
      .style("fill", function(d) { return color(sensorType + '.' + sensorId + ': ' + numPoints); });

  var legend = svg.selectAll(".legend")
      .data(color.domain())
    .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

  legend.append("rect")
      .attr("x", width - 18)
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", color);

  legend.append("text")
      .attr("x", width - 24)
      .attr("y", 9)
      .attr("dy", ".35em")
      .style("text-anchor", "end")
      .text(function(d) { return d; });

});
