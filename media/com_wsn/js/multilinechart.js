var chart = document.getElementsByTagName('multilinechart')[0];
var datafile = chart.getAttribute('src');
var yunit =  chart.getAttribute('yunit');

var main_margin = {top: 20, right: 80, bottom: 100, left: 40},
mini_margin = {top: 430, right: 80, bottom: 20, left: 40},
main_width = 900 - main_margin.left - main_margin.right,
main_height = 500 - main_margin.top - main_margin.bottom,
mini_height = 500 - mini_margin.top - mini_margin.bottom;

var formatDate = d3.time.format("%Y-%m-%d %H:%M:%S"),
parseDate = formatDate.parse,
bisectDate = d3.bisector(function(d) { return d.ts; }).left,
formatOutput0 = function(d) { return formatDate(d.ts) + ": " + d.val + " " + yunit; };
//formatOutput1 = function(d) { return formatDate(d.ts) + ": " + d.Anz; };

var main_x = d3.time.scale()
.range([0, main_width]),
mini_x = d3.time.scale()
.range([0, main_width]);

var main_y0 = d3.scale.linear()
.range([main_height, 0]),
main_y1 = d3.scale.linear()
.range([main_height, 0]),
mini_y0 = d3.scale.linear()
.range([mini_height, 0]),
mini_y1 = d3.scale.linear()
.range([mini_height, 0]);

var dateFormatter = d3.time.format("%m-%d %H:%M");
var main_xAxis = d3.svg.axis()
.scale(main_x)
.tickFormat(function(d) { return dateFormatter(d); })
.orient("bottom"),
mini_xAxis = d3.svg.axis()
.scale(mini_x)
.tickFormat(function(d) { return dateFormatter(d); })
.orient("bottom");

var main_yAxisLeft = d3.svg.axis()
.scale(main_y0)
.orient("left");
main_yAxisRight = d3.svg.axis()
.scale(main_y1)
.orient("right");

var brush = d3.svg.brush()
.x(mini_x)
.on("brush", brush);

var main_line0 = d3.svg.line()
//.interpolate("cardinal")
.x(function(d) { return main_x(d.ts); })
.y(function(d) { return main_y0(d.val); });

var main_line1 = d3.svg.line()
//.interpolate("cardinal")
.x(function(d) { return main_x(d.ts); });
//.y(function(d) { return main_y1(d.Anz); });

var mini_line0 = d3.svg.line()
.x(function(d) { return mini_x(d.ts); })
.y(function(d) { return mini_y0(d.val); });

/*var mini_line1 = d3.svg.line()
.x(function(d) { return mini_x(d.ts); })
.y(function(d) { return mini_y1(d.Anz); });*/

var svg = d3.select("multilinechart").append("svg")
.attr("width", main_width + main_margin.left + main_margin.right)
.attr("height", main_height + main_margin.top + main_margin.bottom);

svg.append("defs").append("clipPath")
.attr("id", "clip")
.append("rect")
.attr("width", main_width)
.attr("height", main_height);

var main = svg.append("g")
.attr("transform", "translate(" + main_margin.left + "," + main_margin.top + ")");

var mini = svg.append("g")
.attr("transform", "translate(" + mini_margin.left + "," + mini_margin.top + ")");

d3.csv(datafile, function(error, data) {
data.forEach(function(d) {
d.ts = parseDate(d.ts);
d.val = +d.val;
//d.Anz = +d.Anz;
});

data.sort(function(a, b) {
return a.ts - b.ts;
});

main_x.domain([data[0].ts, data[data.length - 1].ts]);
main_y0.domain(d3.extent(data, function(d) { return d.val; }));
//main_y0.domain([0.1, d3.max(data, function(d) { return d.val; })]);
//main_y1.domain(d3.extent(data, function(d) { return d.Anz; }));
mini_x.domain(main_x.domain());
mini_y0.domain(main_y0.domain());
//mini_y1.domain(main_y1.domain());

main.append("path")
  .datum(data)
  .attr("clip-path", "url(#clip)")
  .attr("class", "line line0")
  .attr("d", main_line0);

main.append("path")
  .datum(data)
  .attr("clip-path", "url(#clip)")
  .attr("class", "line line1")
  .attr("d", main_line1);

main.append("g")
  .attr("class", "x axis")
  .attr("transform", "translate(0," + main_height + ")")
  .call(main_xAxis);
/*  .selectAll("text")  
    .style("text-anchor", "end")
    .attr("dx", "-.8em")
    .attr("dy", ".15em")
    .attr("transform", function(d) {
        return "rotate(-15)" 
        });*/
main.append("g")
  .attr("class", "y axis axisLeft")
  .call(main_yAxisLeft)
.append("text")
  .attr("transform", "rotate(-90)")
  .attr("y", 6)
  .attr("dy", ".71em")
  .style("text-anchor", "end")
  .text(yunit);

/*main.append("g")
  .attr("class", "y axis axisRight")
  .attr("transform", "translate(" + main_width + ", 0)")
  .call(main_yAxisRight)
.append("text")
  .attr("transform", "rotate(-90)")
  .attr("y", -12)
  .attr("dy", ".71em")
  .style("text-anchor", "end")
  .text("Anzahl");*/

mini.append("g")
  .attr("class", "x axis")
  .attr("transform", "translate(0," + mini_height + ")")
  .call(main_xAxis);

mini.append("path")
  .datum(data)
  .attr("class", "line")
  .attr("d", mini_line0);

/*mini.append("path")
  .datum(data)
  .attr("class", "line")
  .attr("d", mini_line1);*/

mini.append("g")
  .attr("class", "x brush")
  .call(brush)
.selectAll("rect")
  .attr("y", -6)
  .attr("height", mini_height + 7);

var focus = main.append("g")
  .attr("class", "focus")
  .style("display", "none");

// Anzeige auf der Zeitleiste
focus.append("line")
  .attr("class", "x")
  .attr("y1", main_y0(0) - 6)
  .attr("y2", main_y0(0) + 6)

// Anzeige auf der linken Leiste
focus.append("line")
  .attr("class", "y0")
  .attr("x1", main_width - 6) // nach links
  .attr("x2", main_width + 6); // nach rechts

// Anzeige auf der rechten Leiste
/*focus.append("line")
  .attr("class", "y1")
  .attr("x1", main_width - 6)
  .attr("x2", main_width + 6);*/

focus.append("circle")
  .attr("class", "y0")
  .attr("r", 4);

focus.append("text")
  .attr("class", "y0")
  .attr("dy", "-1em");

/*focus.append("circle")
  .attr("class", "y1")
  .attr("r", 4);

focus.append("text")
  .attr("class", "y1")
  .attr("dy", "-1em");*/

main.append("rect")
  .attr("class", "overlay")
  .attr("width", main_width)
  .attr("height", main_height)
  .on("mouseover", function() { focus.style("display", null); })
  .on("mouseout", function() { focus.style("display", "none"); })
  .on("mousemove", mousemove);

/*var insertLinebreaks = function (d) {
    var el = $(d3.select(this).node());
    var fields = d.split(" ");
    el.text('');
    d3.select(el).append("tspan").text(fields[0]);
    d3.select(el).append("tspan").text(fields[1]);
};*/

//svg.selectAll('g.x.axis g text').each(insertLinebreaks);

function mousemove() {
var x0 = main_x.invert(d3.mouse(this)[0]),
    i = bisectDate(data, x0, 1),
    d0 = data[i - 1],
    d1 = data[i],
    d = x0 - d0.ts > d1.ts - x0 ? d1 : d0;
focus.select("circle.y0").attr("transform", "translate(" + main_x(d.ts) + "," + main_y0(d.val) + ")");
focus.select("text.y0").attr("transform", "translate(" + main_x(d.ts) + "," + main_y0(d.val) + ")").text(formatOutput0(d));
//focus.select("circle.y1").attr("transform", "translate(" + main_x(d.ts) + "," + main_y1(d.Anz) + ")");
//focus.select("text.y1").attr("transform", "translate(" + main_x(d.ts) + "," + main_y1(d.Anz) + ")").text(formatOutput1(d));
focus.select(".x").attr("transform", "translate(" + main_x(d.ts) + ",0)");
focus.select(".y0").attr("transform", "translate(" + main_width * -1 + ", " + main_y0(d.val) + ")").attr("x2", main_width + main_x(d.ts));
//focus.select(".y1").attr("transform", "translate(0, " + main_y1(d.Anz) + ")").attr("x1", main_x(d.ts));
}
});

function brush() {
main_x.domain(brush.empty() ? mini_x.domain() : brush.extent());
main.select(".line0").attr("d", main_line0);
main.select(".line1").attr("d", main_line1);
main.select(".x.axis").call(main_xAxis);
}