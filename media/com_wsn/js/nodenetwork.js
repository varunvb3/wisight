var nwfile = document.getElementsByTagName('nodenetwork')[0].getAttribute('src');

var width = window.innerWidth*0.7,
    height = 800;

var color = d3.scale.category10();

var force = d3.layout.force()
    .charge(-120)
    .linkDistance(160)
    .size([width, height]);

var svg = d3.select("nodenetwork").append("svg")
    .attr("width", width)
    .attr("height", height);

d3.json(nwfile, function(error, graph) {
  force
      .nodes(graph.nodes)
      .links(graph.links)
    	.linkDistance(function(d) { return (-d.value*4); })
      .start();

  var link = svg.selectAll(".link")
      .data(graph.links)
    .enter().append("line")
      .attr("class", "link");
      
  link.append("title")
  		.text(function(d) { return d.value; });

  var node = svg.selectAll(".node")
      .data(graph.nodes)
    .enter().append("circle")
      .attr("class", "node")
      .attr("r", 12)
      .style("fill", function(d) { return color(d.group); })
      .call(force.drag);

  node.append("title")
      .text(function(d) { return d.name; });

  force.on("tick", function() {
    link.attr("x1", function(d) { return d.source.x; })
        .attr("y1", function(d) { return d.source.y; })
        .attr("x2", function(d) { return d.target.x; })
        .attr("y2", function(d) { return d.target.y; });

    node.attr("cx", function(d) { return d.x; })
        .attr("cy", function(d) { return d.y; });
  });
});
