function updateFormAndSubmit(formId, sortName, sortOrder)
{
	form = document.getElementById(formId);
	form.sortName.value = sortName;
	form.sortOrder.value = sortOrder;
	if (formId=='nodesForm') form.nodeUid.remove();
	form.submit();
}

// TODO For deletion, use POST rather than GET
function delConfirm(msg)
{
	return confirm(msg + " Are you sure?");
}

function panFilter(urlTarget)
{
	elem = document.getElementById('panFilter');
	$.ajax({
		url: urlTarget,
		data: {
			panFilter: elem.options[elem.selectedIndex].value
		},
		cache: false
	})
		.done(function(rsp) {
			document.open();
			document.write(rsp);
			document.close();
	  	});
}
