<?php

// Make sure we're being called from the command line, not a web interface
if (array_key_exists('REQUEST_METHOD', $_SERVER)) die();

/**
 * This is a CRON script which should be called from the command-line, not the
 * web. For example something like:
 * /usr/bin/php /path/to/site/cli/poller.php
 */

// Set flag that this is a parent file.
define('_JEXEC', 1);
define('DS', DIRECTORY_SEPARATOR);

error_reporting(~E_ALL);
ini_set('display_errors', 0);

// Load system defines
if (file_exists(dirname(dirname(__FILE__)) . '/defines.php'))
{
	require_once dirname(dirname(__FILE__)) . '/defines.php';
}

if (!defined('_JDEFINES'))
{
	define('JPATH_BASE', dirname(dirname(__FILE__)));
	require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.php';
require_once JPATH_LIBRARIES . '/cms.php';
require_once JPATH_BASE . '/components/com_wsn/dbaccess.php';

// Force library to be in JError legacy mode
JError::$legacy = true;

// Load the configuration
require_once JPATH_CONFIGURATION . '/configuration.php';

class Poller extends JApplicationCli
{
	private $processedDataTs = NULL;

	public function doExecute()
	{
		while (True) {
			$this->processRules();
			sleep(1);
		}
	}
	
	public function processRules()
	{
		// Script is just starting: ignore old data
		if ($this->processedDataTs==NULL) {
			$this->processedDataTs = getTableData("SensorData","ts","1 ORDER BY ts DESC LIMIT 1",0);
			return;
		}
		
		// See if new data is available and apply rules on them
		$newData = getTableData("SensorData","sensorUid,ts,data","ts>'".$this->processedDataTs."' ORDER BY ts ASC");
		if ($newData) {
			// echo "Received new data: ".count($newData)."\n";
			foreach ($newData as $nd) {
				$rules = getTableData("Rule,NodeInfo,SensorInfo",
							"Rule.nodeUid,macShortAddr,Rule.sensorUid,sensorId,Rule.type,threshold,notifyPeriod,notifiedTs,mobileNum,ruleUid",
							"Rule.nodeUid=NodeInfo.nodeUid AND Rule.sensorUid=SensorInfo.sensorUid AND actionOn='Cloud' AND Rule.sensorUid=".$nd[0]);
				// echo "  Applicable rules: ".count($rules)."\n";
				if ($rules) {
					foreach ($rules as $r) {
						$devName = $this->getDevName($r[3]);
						$now = new DateTime('now');
						$then = new DateTime($r[7]);
						$diff = $now->diff($then);
						$dsecs = $diff->days*24*3600 + $diff->h*3600 + $diff->i*60 + $diff->s;
						// echo $now->format('Y-m-d H:i:s') . " / " . $then->format('Y-m-d H:i:s') . " / " . $dsecs . " / $nd[2] / $r[5]\n";
						if ($dsecs>$r[6]) { // notifyPeriod exceeded: record a trigger event
							if ($r[4]=="LowThreshold" && $nd[2]<$r[5]) {
								$msg = "Cloud; ShortAddr=0x$r[1]; Sensor=$devName; Below low threshold: $nd[2] < $r[5].";
							}
							else if ($r[4]=="HighThreshold" && $nd[2]>$r[5]) {
								$msg = "Cloud; ShortAddr=0x$r[1]; Sensor=$devName; High threshold exceeded: $nd[2] > $r[5].";
							}
							else continue;
							executeQuery("INSERT INTO EventInfo VALUES($r[0], NOW(), 'Warn', 'RuleTrigger', '$msg')");
							executeQuery("UPDATE Rule SET notifiedTs=NOW() WHERE ruleUid=$r[9]");
							$this->sendSms($r[8], $msg);
						}
					}
				}
				$this->processedDataTs = $nd[1];
			}
		}
	}
	
	public function sendSms($mobile, $msg)
	{
		$transactional = '&gwid=2';
		$url = "http://login.smsgatewayhub.com/smsapi/pushsms.aspx?user=XXXX&pwd=XXXX&to=$mobile&sid=XXXX&msg=$msg&fl=0$transactional";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
	}
	
	public function getDevName($devId)
	{
		switch ($devId)
		{
			case 0x1: return "AT24C01C_1";
			case 0x2: return "AT24MAC602_1";
			case 0x4: return "M24M01_1";
			case 0x9: return "LM75B_1";
			case 0xa: return "TMP102_1";
			case 0xb: return "NTC_THERMISTOR_1";
			case 0xc: return "NTC_THERMISTOR_2";
			case 0x11: return "SFH_7773_1";
			case 0x12: return "TSL45315_1";
			case 0x13: return "MPU6050_1";
			case 0x14: return "ADXL345_1";
			case 0x15: return "SHT10_1";
			case 0x16: return "LIS3MDLTR_1";
			case 0x19: return "CC2520_1";
			case 0x1a: return "CC1200_1";
			case 0x1b: return "CC1101_1";
			case 0x1c: return "CC3100_1";
			case 0x20: return "MPL115A1_1";
			case 0x28: return "MAX_SONAR_1";
			case 0x30: return "EKMC160111X_1";
			case 0x40: return "LED_1";
			case 0x41: return "LED_2";
			case 0x50: return "REED_SWITCH_1";
			case 0x51: return "SPST_SWITCH_1";
			case 0x60: return "MP3V5050GP_1";
			case 0x61: return "HE055T01_1";
			case 0x62: return "MP3V5004GP_1";
			case 0x65: return "LLS_1";
			case 0x68: return "BATT_1";
			case 0x70: return "AD7797_1";
			case 0x78: return "ON_CHIP_VCC_SENSOR";
			case 0x79: return "ON_CHIP_TEMP_SENSOR";
			case 0x80: return "SYNC_RT_1";
			case 0xb0: return "CC2D33S_1";
			case 0x90: return "GPIO_REMOTE_CTRL";
			case 0x91: return "CHIRP_PWLA_1";
			case 0x92: return "FC_28_1";
			case 0x93: return "WSMS100_1";
			case 0xb8: return "MPL3115A2_1";
			case 0xb9: return "MPL115A2_1";
			case 0xba: return "BMP180_1";
			case 0xc0: return "MMA7660FC_1";
			case 0xe0: return "I2C_SW_BUS_1";
			case 0xe1: return "I2C_SW_BUS_2";
			case 0xe8: return "SPI_HW_BUS_1";
			case 0xfe: return "32KHZ_CRYSTAL";
			case 0xff: return "GENERIC";
			case 0xa0: return "UART_HW_1";
			default: return "UNKNOWN";
		}
	}
}

JApplicationCli::getInstance('Poller')->execute();
