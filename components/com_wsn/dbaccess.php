<?php

/* ensure that this file is called by another file */
defined('_JEXEC') or die('Restricted access');

function getQueryResult($db, $resultType)
{
    switch ($resultType) {
    	case 0:
    		$result = $db->loadResult();
    		break;
    	case 1:
    		$result = $db->loadRow();
    		break;
    	case 3:
    		$result = $db->loadColumn();
    		break;
    	case 2:
    	default:
    		$result = $db->loadRowList();
    		break;
    }
    return $result;
}

function executeTransaction($queries)
{
	try {
		$db = JFactory::getDBO();
		$db->transactionStart();
		foreach ($queries as $query) {
			$db->setQuery($query);
			$db->query();
		}
		$db->transactionCommit();
	}
	catch (Exception $e) {
		$db->transactionRollback();
	}
}

function executeQuery($query, $resultType=2)
{
	$db = JFactory::getDBO();
	//echo "$query<br>";
	$db->setQuery($query);
	return getQueryResult($db, $resultType);
}

function getTableData($tablename, $fields='*', $match=1, $resultType=2)
{
	$db = JFactory::getDBO();
	$query = "SELECT $fields FROM $tablename WHERE $match";
	//echo "$query<br>";
	$db->setQuery($query);
	return getQueryResult($db, $resultType);
}

function getEnumValue($tablename, $field)
{
	$db = JFactory::getDBO();
	$query = "SHOW COLUMNS FROM {$tablename} WHERE Field = '{$field}'";
	$db->setQuery($query);
	$row = getQueryResult($db, 1);
	preg_match("/^enum\(\'(.*)\'\)$/", $row[1], $matches);
	$enum = explode("','", $matches[1]);
	return $enum;
}
