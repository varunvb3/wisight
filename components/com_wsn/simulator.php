<?php

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once( JPATH_COMPONENT.DS.'dbaccess.php');

function updateNwInfo()
{
	$isPresent = getTableData("NetworkInfo", "COUNT(*)", "panId='7455'", 0);
	if ($isPresent==0) {
		executeQuery("INSERT INTO NetworkInfo VALUES('7455', 0, 2405, 'DEADBEEFFEEDDADD', 5)", 0);
		executeQuery("INSERT INTO NodeInfo (name,macAddr,macShortAddr,panId)"
			. "VALUES('LPWMN Coord','DEADBEEFFEEDDADD','0001','7455')");
		executeQuery("INSERT INTO EventInfo (nodeUid, ts, severity, type, msg) VALUES(1, DATE_SUB(NOW(),INTERVAL 13 HOUR), 'Info', 'GwBoot', 'Network gateway is just coming up.')");
	}
}

function updateNodes()
{
	$fixedNw = 1;
	$maxNwSize = 25;

	// Delete nodes: probably doesn't happen in a real network, unless operator does it
	$result = getTableData("NodeInfo", "macAddr,macShortAddr,lastHopRssi", "1");
	$numNodes = count($result);
	if ($fixedNw) {
		if ($numNodes>$maxNwSize) $numDelete = $numNodes-$maxNwSize;
		else $numDelete = 0;
	}
	else $numDelete = rand(0,$numNodes);
	for ($i=0; $i<$numDelete; $i++) {
		do {
			$thisDelete = rand(0,$numNodes-1);
		} while (in_array($thisDelete, $deleted));
		$deleted[] = $thisDelete;
		executeQuery("DELETE FROM EventInfo WHERE nodeUid=(SELECT nodeUid FROM NodeInfo WHERE macAddr='".$result[$thisDelete][0]."')");
		executeQuery("DELETE FROM SensorInfo WHERE nodeUid=(SELECT nodeUid FROM NodeInfo WHERE macAddr='".$result[$thisDelete][0]."')");
		executeQuery("DELETE FROM NodeInfo WHERE macAddr='".$result[$thisDelete][0]."'");
	}

	// Add nodes
	$locations = array('Blk-A', 'Blk-B', 'North Wing', 'South Wing', 'Corridor-A', 'Corridor-B', 'Nw Lab', 'ECE Dept', 'Car Park', 'Auditorium');
	$powerSrcs = array('Mains', 'Battery', 'Solar');
	$states =	array('Associated', 'Active', 'Standby');
	$nextShortAddr = executeQuery("SELECT MAX(CONV(macShortAddr,16,10)+0) FROM NodeInfo WHERE 1",0);
	if ($fixedNw) {
		if ($numNodes<$maxNwSize) $numAdd = $maxNwSize-$numNodes;
		else $numAdd = 0;
	}
	else $numAdd = rand(0,($maxNwSize-$numNodes+$numDelete)/2); // division, so that network grows slowly
	for ($i=0; $i<$numAdd; $i++) {
		$nextShortAddr++;
		if ($nextShortAddr==1) $nextShortAddr++; // reserve 0x0001 for coordinator
		$hexShortAddr = sprintf("%04X", $nextShortAddr);
		$nextMacAddr = "$hexShortAddr$hexShortAddr$hexShortAddr$hexShortAddr";
		$numSensors = rand(0,6);
		$currLocation = rand(0,count($locations)-1);
		$currPowerSrc = rand(0,count($powerSrcs)-1);
		$currState = rand(0,count($states)-1);
		executeQuery("INSERT INTO NodeInfo (name,location,macAddr,macShortAddr,panId,lastHopRssi,lastHopCorr,powerSrc,state)"
			. " VALUES('$hexShortAddr','$locations[$currLocation]','$nextMacAddr','$hexShortAddr','7455',".getRssi().",".getCorrelation().",'$powerSrcs[$currPowerSrc]','$states[$currState]')");
		$nodeUid = getTableData('NodeInfo','nodeUid',"macAddr='$nextMacAddr'", 0);
		addNodeSensors($nodeUid, $numSensors); // maximum of six sensors per node
		updateRoute($nodeUid,$nextShortAddr,$states[$currState]);
		addNodeInitEvents($nodeUid);
		updateNodeSensorData($nodeUid);
	}

	executeQuery("UPDATE NetworkInfo SET nodeCount=(SELECT COUNT(*) FROM NodeInfo) WHERE panId='7455'");
}

function getRssi()
{
	return -60 + rand(-30,30);
}

function getCorrelation()
{
	return 50 + rand(-30,30);
}

function addNodeSensors($nodeUid, $numSensors)
{
	$periods = array('30 secs', '1 min', '2 mins', '5 mins', '1 hour', '2 hours', '3 hours');
	$modes = array('Analog', 'Digital');
	$types = array('Voltage', 'Temperature', 'Motion', 'Switch', 'Pressure');
	$mfrs = array('Voltage'=>'TI', 'Temperature'=>'NXP', 'Motion'=>'Panasonic', 'Switch'=>'Standex', 'Pressure'=>'Freescale');
	$partNums = array('Voltage'=>'MSP430', 'Temperature'=>'LM75B', 'Motion'=>'EKMC1601112', 'Switch'=>'ORD324', 'Pressure'=>'MPXV5010GC6T1');
	$units = array('Voltage'=>'Volt', 'Temperature'=>'Celsius', 'Motion'=>'Counter', 'Switch'=>'Counter', 'Pressure'=>'Pascal');
	$scales = array('Voltage'=>'Milli', 'Temperature'=>'Centi', 'Motion'=>'Unit', 'Switch'=>'Unit', 'Pressure'=>'Hecto');
	$states = array('Active', 'Standby', 'Shutdown');
	for ($i=1; $i<=$numSensors; $i++) {
		$opMode = rand(0,2);
		$currPushPeriod = rand(0,count($periods)-1);
		$currAlarmPeriod = rand(0,count($periods)-1);
		$currMode = rand(0,count($modes)-1);
		$currType = rand(0,count($types)-1);
		$currState = rand(0,count($states)-1);
		executeQuery("INSERT INTO SensorInfo (nodeUid, sensorId, manufacturer, partNum, activeTime, activePwr, standbyPwr, "
				. "opMode, pushPeriod, alarmPeriod, mode, type, unit, scale, minVal, maxVal, thresholdLow, thresholdHigh, "
				.	"state) "
				. "VALUES($nodeUid, $i, '".$mfrs[$types[$currType]]."', '".$partNums[$types[$currType]]."', '125 us', '-', '-', "
				. "'$opMode', '$periods[$currPushPeriod]', '$periods[$currAlarmPeriod]', '$modes[$currMode]', '$types[$currType]', '".$units[$types[$currType]]."', '".$scales[$types[$currType]]."', 0, 0, 0, 0, "
				.	"'$states[$currState]')", 0);
	}
}

function updateRoute($nodeUid, $shortAddr, $state)
{
	$singleHopNw = 0;

	if ($singleHopNw) {
		executeQuery("INSERT INTO RouteInfo (nodeUid, numHops, route) VALUES($nodeUid, 1, '$nodeUid')");
		return;
	}

	// Hard-coded for a multihop network of 25 nodes: does not include alternative routes
	$NwHops = array('2'=>'2-1', '3'=>'3-2-1', '4'=>'4-5-3-2-1', '5'=>'5-3-2-1', '6'=>'6-2-1',
					'7'=>'7-1', '8'=>'8-7-1', '9'=>'9-11-1', '10'=>'10-9-11-1', '11'=>'11-1',
					'12'=>'12-20-2-1', '13'=>'13-10-9-11-1', '14'=>'14-10-9-11-1', '15'=>'15-16-18-20-2-1', '16'=>'16-18-20-2-1',
					'17'=>'17-18-20-2-1', '18'=>'18-20-2-1', '19'=>'19-20-2-1', '20'=>'20-2-1', '21'=>'21-1',
					'22'=>'22-1', '23'=>'23-22-1', '24'=>'24-2-1', '25'=>'25-24-2-1');
	$route = $NwHops[$shortAddr];
	$hops = explode('-', $route);
	$numHops = count($hops)-1;
	executeQuery("INSERT INTO RouteInfo (nodeUid, numHops, route) VALUES($nodeUid, $numHops, '$route')");
}

function addNodeInitEvents($nodeUid)
{
	$dateMinus = rand(6,12);
	executeQuery("INSERT INTO EventInfo (nodeUid, ts, severity, type, msg) VALUES($nodeUid, DATE_SUB(NOW(),INTERVAL '$dateMinus:20' HOUR_MINUTE), 'Info', 'NodeCommissioning', 'Node identified by its MAC address has been auto-commissioned.')");
	executeQuery("INSERT INTO EventInfo (nodeUid, ts, severity, type, msg) VALUES($nodeUid, DATE_SUB(NOW(),INTERVAL '$dateMinus:9' HOUR_MINUTE), 'Info', 'NodeAssoc', 'Node associated with PAN coordinator.')");
	executeQuery("INSERT INTO EventInfo (nodeUid, ts, severity, type, msg) VALUES($nodeUid, DATE_SUB(NOW(),INTERVAL '$dateMinus:5' HOUR_MINUTE), 'Info', 'NodeRouteDiscovery', 'Node route has been received.')");
}

function updateNodeEvents($nodeUid)
{
	$probAdd = rand(0,100);
	if ($probAdd<15) { // Add rarely for simulation purpose
		$randNum = rand(0,1);
		if ($randNum) {
			executeQuery("INSERT INTO EventInfo (nodeUid, ts, severity, type, msg) VALUES($nodeUid, DATE_SUB(NOW(),INTERVAL 1 SECOND), 'Error', 'NodeDisAssoc', 'Node disassociated in an unexpected manner.')");
			executeQuery("INSERT INTO EventInfo (nodeUid, ts, severity, type, msg) VALUES($nodeUid, NOW(), 'Info', 'NodeAssoc', 'Node associated with PAN coordinator.')");
		}
		else executeQuery("INSERT INTO EventInfo (nodeUid, ts, severity, type, msg) VALUES($nodeUid, NOW(), 'Info', 'NodeRouteDiscovery', 'Node route has been received.')");
		executeQuery("UPDATE NodeInfo SET lastHopRssi='".getRssi()."', lastHopCorr='".getCorrelation()."' WHERE nodeUid=$nodeUid");
	}
}

function updateAllEvents()
{
	$result = getTableData("NodeInfo", "nodeUid", "nodeUid!=1");
	$selectedNode = rand(0,count($result)-1);
	updateNodeEvents($result[$selectedNode][0]);
}

function getPeriodDateIntervalFormat($period)
{
	if (preg_match("/(\d+) (sec|min|mins|hour|hours)/",$period,$matches)) {
		if ($matches[2]=='sec') return "PT".$matches[1]."S";
		if ($matches[2]=='min' || $matches[2]=='mins') return "PT".$matches[1]."M";
		if ($matches[2]=='hour' || $matches[2]=='hours') return "PT".$matches[1]."H";
	}
}

function getPeriodSecs($period)
{
	if (preg_match("/(\d+) (sec|min|mins|hour|hours)/",$period,$matches)) {
		if ($matches[2]=='sec') return $matches[1];
		if ($matches[2]=='min' || $matches[2]=='mins') return 60*$matches[1];
		if ($matches[2]=='hour' || $matches[2]=='hours') return 3600*$matches[1];
	}
}

function updateNodeSensorData($nodeUid)
{
	$tsAssoc = getTableData('EventInfo','ts',"nodeUid='$nodeUid' AND type='NodeAssoc' ORDER BY ts DESC LIMIT 1", 0);
	$result = getTableData('SensorInfo','sensorUid,type,pushPeriod',"nodeUid='$nodeUid'");
	foreach ($result as $row) {
		$tsSensorData = getTableData('SensorData','ts',"sensorUid='$row[0]' ORDER BY ts DESC LIMIT 1", 0);
		if ($tsSensorData)
			updateSensorData($row[0], $row[1], $tsSensorData, $row[2]);
		else
			updateSensorData($row[0], $row[1], $tsAssoc, $row[2]);
	}
}

function updateSensorData($sensorUid, $type, $from, $period)
{
	if ($type=='Motion' || $type=='Switch') {
		updateAlarmData($sensorUid, $from, $period);
	}
	else {
		updatePeriodicData($sensorUid, $type, $from, $period);
	}
}

function updateAlarmData($sensorUid, $from, $period)
{
	$start = new DateTime($from); $startTs = $start->getTimestamp();
	$end = new DateTime('now'); $endTs = $end->getTimestamp();
	$intervalTs = $endTs - $startTs;
	$numEvents = floor($intervalTs/3600); // one per hour on average

	// Hard-coded time for nextUpdate so that GUI displays a positive value
	$periodFmtd = getPeriodDateIntervalFormat($period);
	$next = new DateTime('now');
	$next->add(new DateInterval($periodFmtd));

	for ($i=0; $i<$numEvents; $i++) {
		$eventOffset = rand(1, $intervalTs-1);
		$curr = clone $start;
		$curr->add(new DateInterval("PT".$eventOffset."S"));
		executeQuery("INSERT INTO SensorData (sensorUid, ts, data) VALUES($sensorUid, '".$curr->format('Y-m-d H:i:s')."', 1)");
		executeQuery("UPDATE NodeInfo,SensorInfo SET lastHopRssi='".getRssi()."', "
			."lastHopCorr='".getCorrelation()."', "
			."lastUpdate='".$curr->format('Y-m-d H:i:s')
			."' WHERE sensorUid=$sensorUid AND NodeInfo.nodeUid=SensorInfo.nodeUid");
	}
}

function updatePeriodicData($sensorUid, $type, $from, $period)
{
	$values = array('Voltage'=>'2943', 'Temperature'=>'2649', 'Pressure'=>'1013');
	$valueVariance = array('Voltage'=>'100', 'Temperature'=>'500', 'Pressure'=>'50');
	$start = new DateTime($from);
	$end = new DateTime('now');
	$curr = new DateTime('now');

	// Hard-coded time for nextUpdate so that GUI displays a positive value
	$periodFmtd = getPeriodDateIntervalFormat($period);
	$next = new DateTime('now');
	$next->add(new DateInterval($periodFmtd));

	$timeDiff = getTableData('SensorData','TIMESTAMPDIFF(SECOND,ts,NOW())', "sensorUid=$sensorUid ORDER BY ts DESC LIMIT 1", 0);
	if ($timeDiff) { // entries exist: update them
		if ($timeDiff>=getPeriodSecs($period)) {
			executeQuery("UPDATE SensorData SET data=data+(RAND()-0.5)*".$valueVariance[$type]
				.", ts=DATE_ADD(ts,INTERVAL $timeDiff SECOND) WHERE sensorUid=$sensorUid");
			executeQuery("UPDATE NodeInfo,SensorInfo SET lastHopRssi='".getRssi()."', "
				."lastHopCorr='".getCorrelation()."', "
				."lastUpdate='".$curr->format('Y-m-d H:i:s')."', "
				."nextUpdate='".$next->format('Y-m-d H:i:s')
				."' WHERE sensorUid=$sensorUid AND NodeInfo.nodeUid=SensorInfo.nodeUid");
		}
		return; // don't add new data
	}

	// add data for the first time
	for ($i=0; $i<150 && $curr->format('U')>$start->format('U'); $i++) { // only few points for performance reasons
		//echo "$periodFmtd: ".$start->format('Y-m-d H:i:s U')." -> ".$end->format('Y-m-d H:i:s U')."<br>";
		$currValue = $values[$type] + rand(-$valueVariance[$type], $valueVariance[$type]);
		executeQuery("INSERT INTO SensorData (sensorUid, ts, data) VALUES($sensorUid, '".$curr->format('Y-m-d H:i:s')."', $currValue)");
		if ($i==0) executeQuery("UPDATE NodeInfo,SensorInfo SET lastHopRssi='".getRssi()."', "
			."lastHopCorr='".getCorrelation()."', "
			."lastUpdate='".$curr->format('Y-m-d H:i:s')."', "
			."nextUpdate='".$next->format('Y-m-d H:i:s')
			."' WHERE sensorUid=$sensorUid AND NodeInfo.nodeUid=SensorInfo.nodeUid");
		$curr->sub(new DateInterval($periodFmtd));
	}
}

function updateAllSensorData()
{
	// TODO For performance reasons, do nothing.
	echo "<div class='box-info' style='padding-left:40px'>For the purpose of this demo, sensor data is updated only when accessing specific node or sensor information. Hence you may sometimes see negative values for the <em>Next Update</em> column.</div>";
}

