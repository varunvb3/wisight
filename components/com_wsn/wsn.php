<?php
/**
* Author:	Omar Muhammad
* Email:	admin@omar84.com
* Website:	http://omar84.com
* Component:wsn
* Version:	1.7.0
* Date:		21/9/2011
* copyright	Copyright (C) 2011 http://omar84.com. All Rights Reserved.
* @license	http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once(JPATH_COMPONENT.DS.'dbaccess.php');
require_once(JPATH_COMPONENT.DS.'utils.php');
require_once(JPATH_COMPONENT.DS.'simulator.php');
require_once(JPATH_COMPONENT.DS.'actions.php');

$Simulation = 0;

$app = JFactory::getApplication();
$admin = $app->isAdmin();
if($admin==1) {
?>
<div>
	This Component was made to make it possible to create a menu item that has only modules and no component.<br />
	You can use it by adding a new menu item, select "wsn" from the "Menu Item Type" list, and save.<br />
	then, go to the module manager, and assign the modules you want to use with this menu item, and you're done!<br />
</div>
<?php
}
else {
	jimport('joomla.application.component.controller');

	// Create the controller
	$controller = JController::getInstance('Wsn');

	// Perform the Request task
	$controller->execute(JRequest::getCmd('task'));
	
	// Set cookies
	setPanCookie(getUserPanIds());
	
	$nodeUid = JRequest::getVar('nodeUid');
	$sensorUid = JRequest::getVar('sensorUid');
	$Itemid = JRequest::getVar('Itemid');
	$action = JRequest::getVar('action');
	$menuItem = getTableData("#__menu", "alias", "id=$Itemid", 0);
	switch ($menuItem) {
		case 'home':
			$user = JFactory::getUser();
			if ($user->guest) break;
			if ($Simulation) {
				echo "<div class='box-info' style='padding-left:40px'>
					This is a demo of the WiSense Sensor-Actuator Network (WISAN) user interface.
					A multihop mesh WISAN is depicted below.</div>";
				updateNwInfo();
				updateNodes();
			}
			if ($action=='nwConfig') networkConfig();
			else if ($action=='coordConfig') coordConfig();
			else showHome();
			break;
		case 'nodes':
			if (isset($nodeUid) && $nodeUid!='') {
				if ($Simulation) {
					updateNodeEvents($nodeUid);
					updateNodeSensorData($nodeUid);
				}
				if ($action=='nodeConfig') {
					nodeConfig($nodeUid);
					if ($_POST) {
						header("Location: " . JURI::base(true).'/index.php'.$_SERVER['PATH_INFO']."?nodeUid=$nodeUid");
						die();
					}
				}
				else if ($action=='nodeDelete') {
					nodeDelete($nodeUid);
					header("Location: " . JURI::base(true)."/index.php/nodes");
					die();
				}
				else if ($action=='digitalIOCtrl') {
					if (isset($nodeUid) && $nodeUid!='')
						digitalIOControl($nodeUid);
					header("Location: " . JURI::base(true)."/index.php/nodes?nodeUid=$nodeUid");
					die();
				}
				else if ($action=='addRule') {
					editRule($nodeUid, 0);
					if ($_POST) {
						header("Location: " . JURI::base(true)."/index.php/nodes?nodeUid=$nodeUid");
						die();
					}
				}
				else if ($action=='editRule') {
					$ruleUid = JRequest::getVar('ruleUid');
					if (isset($ruleUid) && $ruleUid!='')
						editRule($nodeUid, $ruleUid);
					if ($_POST) {
						header("Location: " . JURI::base(true)."/index.php/nodes?nodeUid=$nodeUid");
						die();
					}
				}
				else if ($action=='delRule') {
					$ruleUid = JRequest::getVar('ruleUid');
					if (isset($ruleUid) && $ruleUid!='')
						deleteRule($nodeUid, $ruleUid);
					header("Location: " . JURI::base(true)."/index.php/nodes?nodeUid=$nodeUid");
					die();
				}
				else showNode($nodeUid);
			}
			else if ($action=='nodeDelete') {
				nodeDelete();
				header("Location: " . JURI::base(true)."/index.php/nodes");
				die();
			}
			else showNodes();
			break;
		case 'sensors':
			if (isset($sensorUid) && $sensorUid!='') {
	  			if ($Simulation) {
					$row = getTableData('SensorInfo','nodeUid,type,pushPeriod',"sensorUid=$sensorUid", 1);
	  				$tsAssoc = getTableData('EventInfo','ts',"nodeUid='$row[0]' AND type='NodeAssoc' ORDER BY ts DESC LIMIT 1", 0);
					$tsSensorData = getTableData('SensorData','ts',"sensorUid='$sensorUid' ORDER BY ts DESC LIMIT 1", 0);
	  				if ($tsSensorData) updateSensorData($sensorUid, $row[1], $tsSensorData, $row[2]);
	  				else updateSensorData($sensorUid, $row[1], $tsAssoc, $row[2]);
	  			}
				showSensor($sensorUid);
			}
			else {
				if ($Simulation) updateAllSensorData();
				showSensors();
			}
			break;
		case 'events':
			if ($Simulation) updateAllEvents();
			showEvents();
			break;
		default:
			break;
	}

	// Redirect if set by the controller
	$controller->redirect();
}

function showPanFilter()
{
	$panIds = getUserPanIds();
	$panId = setPanCookie($panIds);

	$optStr = getOptStr($panIds, $panId);

	$url = JURI::getInstance();
	$currUri = $url->current();

	echo "<span class=panLabel>Select LPWMN</span>";
	echo "<select id=panFilter onchange='panFilter(\"$currUri\");'>";
	echo $optStr;
	echo "</select>";
}

function showNetworkInfo()
{
	$panId = getPanId();
	$row = executeQuery("SELECT NetworkInfo.panId,nodeCount,band,channel,modulation,radioBaudRate FROM NetworkInfo, NodeInfo WHERE NetworkInfo.panId='$panId'", 1);
	$numNodes = executeQuery("SELECT COUNT(*) FROM NodeInfo WHERE panId='$panId' AND macShortAddr!='0001'", 0);
	$numSensors = executeQuery("SELECT COUNT(*) FROM NodeInfo, SensorInfo WHERE NodeInfo.nodeUid=SensorInfo.nodeUid AND panId='$panId'", 0);
	echo getEditUrl("","action=nwConfig",'Edit','<i class="fa fa-edit"></i>','class=headingSideLink');
?>
<table class=nwinfo>
	<tr><th>LPWMN ID</th></tr>
	<tr><td><?php echo "0x".$row[0]; ?></td></tr>
	<tr><th>Node Count</th></tr>
	<tr><td><?php echo $numNodes; ?></td></tr>
	<tr><th>Sensor Count</th></tr>
	<tr><td><?php echo $numSensors; ?></td></tr>
	<tr><th>Radio Band (MHz)</th></tr>
	<tr><td><?php echo preg_replace("/ [KM]?Hz/","",$row[2]); ?></td></tr>
	<tr><th>Channel (MHz)</th></tr>
	<tr><td><?php echo number_format($row[3]/1000000,6); ?></td></tr>
	<tr><th>Modulation</th></tr>
	<tr><td><?php echo $row[4]; ?></td></tr>
	<tr><th>Radio Baud Rate (sps)</th></tr>
	<tr><td style="border-bottom: 0px solid;"><?php echo $row[5]; ?></td></tr>
</table>
<?php
}

function showCoordInfo()
{
	$panId = getPanId();
	$row = executeQuery("SELECT coordMacAddr,coordTxPower/100,DATE_FORMAT(buildDate,'%b %d %Y'),TIME_FORMAT(buildTime,'%T'),nodeAssocState FROM NetworkInfo,NodeInfo WHERE NetworkInfo.panId='$panId' AND NodeInfo.macShortAddr='0001'", 1);
	$numWhiteNodes = executeQuery("SELECT COUNT(macAddr) FROM NodeWhiteList", 0);
	echo getEditUrl("","action=coordConfig",'Edit','<i class="fa fa-edit"></i>','class=headingSideLink');
?>
<table class=nwinfo>
	<tr><th>MAC Address</th></tr>
	<tr><td><?php echo "0x".$row[0]; ?></td></tr>
	<tr><th>Tx Power (dBm)</th></tr>
	<tr><td><?php echo $row[1]; ?></td></tr>
	<tr><th>Build Date</th></tr>
	<tr><td><?php echo $row[2] . ', ' . $row[3]; ?></td></tr>
	<tr><th>Node Association</th></tr>
	<tr><td><?php echo $row[4]; ?></td></tr>
	<tr><th>White List</th></tr>
	<tr>
		<td style="border-bottom: 0px solid;">
		<?php
			if ($numWhiteNodes==1) echo "$numWhiteNodes node";
			else if ($numWhiteNodes) echo "$numWhiteNodes nodes";
			else echo "None";
		?>
		</td>
	</tr>
	</table>
<?php
}

function getTableHeaderFields($tableName)
{
    switch ($tableName) {
		case 'nodeinfo':
			$fields[] = "Name|Location|MAC Address|Short Address|RSSI (dBm)|State";
			$fields[] = "name|location|macAddr|macShortAddr|lastHopRssi|state|nodeUid";
			break;
		case 'sensorinfo':
			$fields[] = "Node Name|Sensor Id|Manufacturer|Part Number|Type|Scale|Unit|Value|Last Update|Next Update|State";
			$fields[] = "name|sensorId|manufacturer|partNum|type|scale|unit|SensorInfo.sensorUid|CONVERT_TZ(lastUpdate,@@session.time_zone,'+05:30')|CONVERT_TZ(nextUpdate,@@session.time_zone,'+05:30')|SensorInfo.state"
  						. "|TIMESTAMPDIFF(MINUTE,lastUpdate,NOW())|TIMESTAMPDIFF(MINUTE,NOW(),nextUpdate)|SensorInfo.nodeUid|location|macAddr|macShortAddr"; // not in header
			break;
		case 'rule':
			$fields[] = "Sensor Id|Type|Threshold|Action On|Notify Period (secs)|SMS To";
			$fields[] = "sensorId|Rule.type|threshold|actionOn|notifyPeriod|mobileNum"
						. "|ruleUid|Rule.sensorUid"; // not in header
			break;
		case 'eventinfo':
			$fields[] = "Node Name|MAC Address|Timestamp|Severity|Type|Message";
			$fields[] = "name|macAddr|CONVERT_TZ(ts,@@session.time_zone,'+05:30')|severity|type|msg"
						. "|EventInfo.nodeUid|macShortAddr";
			break;
    }

	return $fields;
}

function getTableColUrl($formId, $colDisplayName, $colName, $sortName, $sortOrder)
{
	$sortimg = "";
    $colName = preg_replace("/CONVERT_TZ\((\w+),.*\)/","$1",$colName);
	if (strcasecmp($colName,$sortName)==0) {
		if (strcasecmp($sortOrder,'DESC')==0) {
			$sortimg = "/media/system/images/sort_desc.png";
			$sortOrder = "ASC";
	  }
		else if (strcasecmp($sortOrder,'ASC')==0) {
	  	$sortimg = "/media/system/images/sort_asc.png";
			$sortOrder = "DESC";
	  }
		else $sortOrder = "ASC";
	}
	else $sortOrder = "ASC";

  echo "<th><a href=\"javascript:;\" onclick=\"updateFormAndSubmit('$formId','$colName','$sortOrder');\">";
	echo "$colDisplayName ";
	if ($sortimg!="") echo "<img src='".JURI::base()."$sortimg' />";
	echo "</a></th>";

}

function printNwDataJson($filename)
{
	$fh = fopen(JPATH_SITE."/$filename", 'w') or die("Can't open file for saving network data into JSON file.");

	fwrite($fh, "{\n");

	fwrite($fh, "  \"nodes\":[\n");
	$panId = getPanId();
	$nodeMap = array();
	$result = getTableData("NodeInfo", "macShortAddr,state,lastHopRssi,nodeUid", "panId='$panId' ORDER BY nodeUid");
	if ($result) {
		for ($i=0; $i<count($result); $i++) {
			if ($result[$i][0]=='0001') $json = "    {\"name\":\"Coordinator\",\"group\":1}";
			else if ($result[$i][1]=='Active') $json = "    {\"name\":\"".$result[$i][0]."\",\"group\":2}";
			else $json = "    {\"name\":\"".$result[$i][0]."\",\"group\":3}";
			if($i==count($result)-1) fwrite($fh, "$json\n");
			else fwrite($fh, "$json,\n");
			$nodeMap[$result[$i][3]] = 
				array('index'=>$i, 'lastHopRssi'=>$result[$i][2]);
		}
	}
	fwrite($fh, "  ],\n");

	$routes = getTableData("RouteInfo", "route", "nodeUid IN (".implode(',',array_keys($nodeMap)).")", 3);
	$links = array();
	foreach ($routes as $route) {
		$hops = explode('-', $route);
		for ($j=0; $j<count($hops)-1; $j++) {
			$src = $hops[$j]; $dst = $hops[$j+1];
			$isrc = $nodeMap[$src]['index']; $idst = $nodeMap[$dst]['index'];
			if (!isset($AllHops["$isrc-$idst"])) {
				// TODO All hops should not use same value as the last hop to coordinator
				$links[] = "    {\"source\":$isrc,\"target\":$idst,\"value\":".$nodeMap[$src]['lastHopRssi']."}";
				$AllHops["$isrc-$idst"] = 1;
			}
	    }
	}
	fwrite($fh, "  \"links\":[\n");
	if (count($links)) fwrite($fh, implode(",\n",$links)."\n");
	fwrite($fh, "  ]\n");
	
	fwrite($fh, "}\n");

	fclose($fh);
}

function showHome()
{
	$pathPrefix = JURI::base(true);
	$jsonFile = "wsn.json";
	printNwDataJson($jsonFile);
	echo "<nodenetwork src='$pathPrefix/$jsonFile'>";
	echo "<script src='$pathPrefix/media/d3/d3.v3.min.js'></script>";
	echo "<script src='$pathPrefix/media/com_wsn/js/nodenetwork.js'></script>";
}

function showChart($sensorUid, $displayUnit, $scaleFactor, $unit, $type, $sensorId)
{
	$pathPrefix = JURI::base(true);
	$csvFile = "/data$sensorUid.csv";
	$numPoints = printSensorDataCsv($csvFile,$sensorUid,$scaleFactor);
	$chartType = getChartType($unit);
	echo "<$chartType src='$pathPrefix/$csvFile' yunit='$displayUnit' sensorType='$type' sensorId='$sensorId' numPoints='$numPoints'>";
	echo "<script src='$pathPrefix/media/d3/d3.v3.min.js'></script>";
	echo "<script src='$pathPrefix/media/com_wsn/js/$chartType.js'></script>";
}

function getChartType($type)
{
	if ($type=='Counter') return 'scatterplot';
	else return 'multilinechart';
}

function showNode($nodeUid)
{
	$panId = getPanId();
	$formId='nodeinfo';
	
	$url = JURI::getInstance();
	$currUri = $url->current();
	
	echo "<h2>Node Information</h2>";
	echo "<div class='headingSideLinksPanel'>";
	echo getEditUrl("/nodes", "nodeUid=$nodeUid&action=nodeConfig", 'Edit', '<i class="fa fa-edit"></i>');
	echo getEditUrl("/nodes", "nodeUid=$nodeUid&action=nodeDelete", 'Delete', '<i class="delIcon fa fa-trash"></i>', 'onclick="return delConfirm(\'This node and all its associated data will be deleted.\')"');
	echo "</div>";
	echo "<form id=$formId method=get action='$currUri'>";
	echo "<table class=nodeinfo style='width:100%'>";
	$row = getTableData("NodeInfo", "name,location,state,macAddr,macShortAddr,lastHopRssi,lastHopCorr,powerSrc,batteryMAh,pushPeriod", "nodeUid=$nodeUid AND panId='$panId'", 1);
	if ($row) {
		$numSensors = getTableData("NodeInfo,SensorInfo", "COUNT(*)", "NodeInfo.nodeUid=SensorInfo.nodeUid AND NodeInfo.nodeUid=$nodeUid AND panId='$panId'", 0);
		
		echo "<tr><th>Name / Location</th><td>$row[0] / $row[1]</td><th>State</th><td>$row[2]</td></tr>";
		echo "<tr><th>MAC Address</th><td>0x$row[3]</td><th>MAC Short Address</th><td>0x$row[4]</td></tr>";
		echo "<tr><th>Last Hop RSSI (dBm)</th><td>$row[5]</td><th>Last Hop LQI</th><td>$row[6]</td></tr>";
		echo "<tr><th>Number of Sensors</th><td>$numSensors</td><th>Push Period (secs)</th><td>$row[9]</td></tr>";
		echo "<tr><th>Power Source</th><td>$row[7]</td><th>Battery Capacity (mAh)</th><td>$row[8]</td></tr>";

		echo "<tr>";
		echo "<th>Commissioned</th>";
		$tsComm = getTableData("EventInfo", "CONVERT_TZ(ts,@@session.time_zone,'+05:30')", "nodeUid=$nodeUid AND type='NodeCommissioning' ORDER BY ts DESC LIMIT 1", 0);
		if ($tsComm) echo "<td>$tsComm</td>";
		else echo "<td>-</td>";
		echo "<th>Recent Association</th>";
		$tsAssoc = getTableData("EventInfo", "CONVERT_TZ(ts,@@session.time_zone,'+05:30')", "nodeUid=$nodeUid AND type='NodeAssoc' ORDER BY ts DESC LIMIT 1", 0);
		if ($tsAssoc) echo "<td>$tsAssoc</td>";
		else echo "<td>-</td>";
		echo "</tr>";

		echo "</table></form>";
	}
	else {
		echo "<tr><td class=box-warning>Unable to retrieve node information for nodeUid=$nodeUid</td></tr>";
		echo "</table></form>";
		return; // no point continuing
	}

	echo "<div class=intersection></div>";
	showRoute($nodeUid);

	echo "<div class=intersection></div>";
	showSensors($nodeUid);

	$user = JFactory::getUser();
	if (!$user->guest) {
		echo "<div class=intersection></div>";
		showControls($nodeUid);
	}
	
	echo "<div class=intersection></div>";
	showRules($nodeUid);

	echo "<div class=intersection></div>";
	showEvents($nodeUid);
}

function showRoute($nodeUid)
{
	$formId='routeinfo';
	
	$url = JURI::getInstance();
	$currUri = $url->current();
	
	echo "<h2>Route Information</h2>";
	echo "<form id=$formId method=get action='$currUri'>";
	echo "<table class=routeinfo style='width:100%'>";
	$row = getTableData("RouteInfo", "numHops,route", "nodeUid=$nodeUid", 1);
	echo "<tr><th>Number of Hops</th><td>$row[0]</td></tr>";

	$eventRow = getTableData("EventInfo", "CONVERT_TZ(ts,@@session.time_zone,'+05:30'),TIMESTAMPDIFF(MINUTE,ts,NOW())", "nodeUid=$nodeUid AND type='NodeRouteDiscovery' ORDER BY ts DESC LIMIT 1", 1);
	echo "<tr><th>Last Update</th><td>$eventRow[0] ($eventRow[1] mins ago)</td></tr>";

	$hops = explode('-', $row[1]);
	for ($i=0; $i<count($hops); $i++) {
		$hops[$i] = "0x".sprintf("%04X", getTableData("NodeInfo", "macShortAddr", "nodeUid=$hops[$i]", 0));
	}
	$route = implode('-', $hops);
	echo "<tr><th>Route</th><td>$route</td></tr>";

	echo "</table></form>";
}

function showNodes()
{
	$panId = getPanId();
	$formId = "nodesForm";
	$jApp = JFactory::getApplication();
	$frmName = $jApp->input->get('frmName');
	$sortName = $jApp->input->get('sortName');
	if ($frmName!=$formId || !isset($sortName)) $sortName = "macAddr";
	$sortOrder = $jApp->input->get('sortOrder');
	if ($frmName!=$formId || !isset($sortOrder)) $sortOrder = "ASC";

	if (preg_match("/^(state)$/",$sortName)) $sortStr = "CONCAT($sortName)";
	else $sortStr = $sortName;
	
	$fields = getTableHeaderFields('nodeinfo');
	$result = getTableData("NodeInfo", preg_replace("/\|/",",",$fields[1]), "macShortAddr!='0001' AND panId='$panId' ORDER BY $sortStr $sortOrder");
	
	$url = JURI::getInstance();
	$currUri = $url->current();
	
	echo "<h2>Nodes</h2>";
	echo "<div class='headingSideLinksPanel'>";
	if ($result)
		echo getEditUrl("/nodes", "action=nodeDelete", 'Delete All Nodes', '<i class="delIcon fa fa-trash"></i>', 'onclick="return delConfirm(\'This will delete all nodes.\')"');
	echo "</div>";
	echo "<form id=$formId method=get action='$currUri'>";
	echo "<table class=nodelist style='width:100%'>";
	echo "<input type=hidden name=frmName value=$formId />";
	echo "<input type=hidden name=sortName value=macAddr />";
	echo "<input type=hidden name=sortOrder value=ASC />";
	echo "<input type=hidden name=nodeUid />";
	echo "<tr>";
	$tableColNames = explode('|',$fields[0]);
	$dbColNames = explode('|',$fields[1]);
	for ($i=0; $i<count($tableColNames); $i++) {
		getTableColUrl($formId, $tableColNames[$i], $dbColNames[$i],$sortName,$sortOrder);
	}
	echo "</tr>";

	if ($result) {
		foreach ($result as $row) {
			echo "<tr><td><a href=\"".preg_replace("/\?.*/", "", $_SERVER['REQUEST_URI'])."?nodeUid=$row[6]\">$row[0]</a></td>";
			echo "<td>$row[1]</td><td>0x$row[2]</td><td>0x$row[3]</td><td>$row[4]</td><td>$row[5]</td></tr>";
		}
	}
	else {
		echo "<tr><td class='box-warning' style='padding-left:40px' colspan=8>There are no nodes to display.</td></tr>";
	}

	echo "</table></form>";
}

function showSensor($sensorUid)
{
	$formId='sensorinfo';
	
	$url = JURI::getInstance();
	$currUri = $url->current();
	
	echo "<h2>Sensor Information</h2>";
	echo "<form id=$formId method=get action='$currUri'>";
	echo "<table class=sensorinfo style='width:100%'>";
	$row = getTableData("SensorInfo,NodeInfo", "sensorUid,SensorInfo.nodeUid,sensorId,manufacturer,partNum,activeTime,activePwr,standbyPwr,opMode,SensorInfo.pushPeriod,alarmPeriod"
										. ",mode,type,unit,scale,minVal,maxVal,thresholdLow,thresholdHigh,CONVERT_TZ(lastUpdate,@@session.time_zone,'+05:30'),CONVERT_TZ(nextUpdate,@@session.time_zone,'+05:30')"
										. ",SensorInfo.state,macAddr,macShortAddr,TIMESTAMPDIFF(MINUTE,lastUpdate,NOW()),TIMESTAMPDIFF(MINUTE,NOW(),nextUpdate)",
											"SensorInfo.nodeUid=NodeInfo.nodeUid AND sensorUid=$sensorUid", 1);
	if ($row) {
		echo "<tr><th>Sensor Id</th><td>$row[2]</td><th>State</th><td>$row[21]</td></tr>";
		echo "<tr><th>Node MAC Address</th><td><a href=\"".preg_replace("/sensors\?.*/", 'nodes', $_SERVER['REQUEST_URI'])."?nodeUid=$row[1]\">0x$row[22]</td><th>Node MAC Short Address</th><td>0x$row[23]</td></tr>";
		echo "<tr><th>Manufacturer</th><td>$row[3]</td><th>Part Number</th><td>$row[4]</td></tr>";
		echo "<tr><th>Mode</th><td>$row[11]</td><th>Type</th><td>$row[12]</td></tr>";
		echo "<tr><th>Scale</th><td>$row[14]</td><th>Unit</th><td>$row[13]</td></tr>";
		echo "<tr><th>Reporting Mode</th><td>".getOpModeStr($row[8])."</td><th>Active Time</th><td>$row[5]</td></tr>";
		echo "<tr><th>Push Period</th><td>$row[9]</td><th>Alarm Period</th><td>$row[10]</td></tr>";
		echo "<tr><th>Active Power</th><td>$row[6]</td><th>Standby Power</th><td>$row[7]</td></tr>";
		echo "<tr><th>Minimum Value</th><td>$row[15]</td><th>Maximum Value</th><td>$row[16]</td></tr>";
		echo "<tr><th>Low Threshold</th><td>$row[17]</td><th>High Threshold</th><td>$row[18]</td></tr>";

		echo "<tr><th>Last Update</th>";
		if (isset($row[24])) echo "<td>$row[19]<br>($row[24] mins ago)</td>";
		else echo "<td>-</td>";
		echo "<th>Next Update</th>";
		if (isset($row[25])) echo "<td>$row[20]<br>($row[25] mins to go)</td>";
		else echo "<td>-</td>";
		echo "</tr>";
	}
	else {
		echo "<tr><td class='box-warning' style='padding-left:40px'>Unable to retrieve sensor information for sensorUid=$sensorUid</td></tr>";
	}
	echo "</table></form>";

	$displayUnit = getDisplayUnit($row[14], $row[13]);
	$scaleFactor = getDataScalingFactor($displayUnit, $sensorUid, $row[14], $row[13]);
	showChart($sensorUid, $displayUnit, $scaleFactor, $row[13], $row[12], $row[2]);
}

function getDisplayUnit($scale, $unit)
{
	$retVal = '';

	switch ($scale) {
		case 'Tera': $retVal .= 'T'; break;
		case 'Giga': $retVal .= 'G'; break;
		case 'Mega': $retVal .= 'M'; break;
		case 'Kilo': $retVal .= 'k'; break;
		case 'Hecto': $retVal .= 'h'; break;
		case 'Deka': $retVal .= 'D'; break;
		case 'Deci': $retVal .= 'd'; break;
		case 'Centi': $retVal .= 'c'; break;
		case 'Milli': $retVal .= 'm'; break;
		case 'Micro': $retVal .= 'µ'; break;
		case 'Pico': $retVal .= 'p'; break;
		case 'Femto': $retVal .= 'f'; break;
	}

	switch ($unit) {
		case 'Volt': $retVal .= 'V'; break;
		case 'Celsius': $retVal .= '°C'; break;
		case 'Fahrenheit': $retVal .= '°F'; break;
		case 'Pascal': $retVal .= 'Pa'; break;
		case 'Percent': $retVal .= '%'; break;
	}

	return $retVal;
}

function getDataScalingFactor(&$displayUnit, $sensorUid, $scale, $unit, $sensorId)
{
	// TODO Enhance data scaling to handle all cases
	if ($scale=='Centi') {
		$displayUnit = preg_replace("/^\w/","",$displayUnit);
		return 0.01;
	}

	if ($scale=='Milli') {
		$displayUnit = preg_replace("/^\w/","",$displayUnit);
		return 0.001;
	}

	return 1;
}

function printSensorDataCsv($filename, $sensorUid, $scaleFactor)
{
	$fh = fopen(JPATH_SITE."/$filename", 'w') or die("Can't open file for saving network data into CSV file.");

	fwrite($fh, "ts,val\n");
	$result = getTableData("SensorData", "CONVERT_TZ(ts,@@session.time_zone,'+05:30'),data*$scaleFactor", "sensorUid=$sensorUid ORDER BY ts ASC");
	if ($result) {
		for ($i=0; $i<count($result); $i++) {
			fwrite($fh, $result[$i][0].",".$result[$i][1]."\n");
		}
	}
	fclose($fh);

	return count($result);
}

function getOpModeStr($opMode)
{
	$str = "Pull";
	if ($opMode & 1) $str .= ", Push-Periodic";
	if ($opMode & 2) $str .= ", Push-Alarm";
	return $str;
}

function showSensors($nodeUid=0)
{
	$panId = getPanId();
	$formId = "sensorsForm";
	$jApp = JFactory::getApplication();
	$frmName = $jApp->input->get('frmName');
	$sortName = $jApp->input->get('sortName');
	if ($frmName!=$formId ||!isset($sortName)) $sortName = "lastUpdate";
	$sortOrder = $jApp->input->get('sortOrder');
	if ($frmName!=$formId ||!isset($sortOrder)) $sortOrder = "DESC";

	$url = JURI::getInstance();
	$currUri = $url->current();
	
	if ($nodeUid==0) { // show sensors of all nodes
		$filter = "";
	}
	else {
		$filter = "SensorInfo.nodeUid=$nodeUid AND ";
	}

	echo "<h2>Sensors</h2>";
	echo "<form id=$formId method=get action='$currUri'>";
	echo "<table class=sensorlist style='width:100%'>";
	echo "<input type=hidden name=frmName value=$formId />";
	echo "<input type=hidden name=sortName />";
	echo "<input type=hidden name=sortOrder />";
	if ($nodeUid) echo "<input type=hidden name=nodeUid value=$nodeUid />";
	echo "<tr>";
	$fields = getTableHeaderFields('sensorinfo');
	$tableColNames = explode('|',$fields[0]);
	$dbColNames = explode('|',$fields[1]);
	for ($i=0; $i<count($tableColNames); $i++) {
		if ((($nodeUid && $i>0) || $nodeUid==0) && $i!=10) { // don't display state for now
			if ($tableColNames[$i]=='Value') echo "<th>$tableColNames[$i]</th>";
			else getTableColUrl($formId, $tableColNames[$i], $dbColNames[$i], $sortName, $sortOrder);
		}
	}
	echo "</tr>";
	if (preg_match("/^(type|scale|unit)$/",$sortName)) $sortStr = "CONCAT($sortName)";
	else $sortStr = $sortName;
	$result = getTableData("NodeInfo,SensorInfo", preg_replace("/\|/",",",$fields[1]), "$filter NodeInfo.nodeUid=SensorInfo.nodeUid AND panId='$panId' ORDER BY $sortStr $sortOrder");
	if ($result) {
		$sensorUids = array();
		foreach ($result as $row) $sensorUids[] = $row[7];
		
		$values = executeQuery("SELECT SensorData.sensorUid,SUM(data) FROM SensorData,SensorInfo WHERE SensorData.sensorUid=SensorInfo.sensorUid AND SensorData.sensorUid IN (".implode(',',$sensorUids).") AND unit='Counter' GROUP BY SensorData.sensorUid "
					."UNION SELECT sd.sensorUid,data FROM (SELECT * FROM SensorData ORDER BY ts DESC) AS sd,SensorInfo WHERE sd.sensorUid=SensorInfo.sensorUid AND sd.sensorUid IN (".implode(',',$sensorUids).") AND unit!='Counter'  GROUP BY sd.sensorUid");
		$sensorUids = array();
		foreach ($values as $val) $sensorUids[$val[0]] = $val[1];
		
		foreach ($result as $row) {
			echo "<tr>";
			if ($nodeUid==0) echo "<td><a class=tooltip href=\"".preg_replace("/sensors\??.*/", 'nodes', $_SERVER['REQUEST_URI'])."?nodeUid=$row[13]\">$row[0]"
									."<span><img class='callout' src='".JURI::base(true)."/images/callout.gif' >Location: $row[14]<br>MAC Address: 0x$row[15]<br>Short Address: 0x$row[16]</span></a></td>";
			echo "<td><a href=\"".preg_replace("/(nodes|sensors)\??.*/",'sensors',$_SERVER['REQUEST_URI'])."?sensorUid=$row[7]\">$row[1]</a></td>";
			echo "<td>$row[2]</td><td>$row[3]</td><td>$row[4]</td><td>$row[5]</td><td>$row[6]</td>";
			echo "<td>".$sensorUids[$row[7]]."</td>";

			if (isset($row[11])) echo "<td>$row[8]<br>($row[11] mins ago)</td>";
			else echo "<td>-</td>";
			if (isset($row[12])) echo "<td>$row[9]<br>($row[12] mins to go)</td>";
			else echo "<td>-</td>";

			echo "</tr>";
		}
	}
	else {
		if ($nodeUid==0) $msg = 'There are no sensors to display.';
		else $msg = 'There are no sensors attached to this node.';
		echo "<tr><td class='box-warning' style='padding-left:40px' colspan=11>$msg</td></tr>";
	}
	echo "</table></form>";
}

function showControls($nodeUid)
{
	echo "<h2>Controls</h2>";
	digitalIOControl($nodeUid);
}

function showRules($nodeUid)
{
	$formId = "rulesForm";
	$jApp = JFactory::getApplication();
	$frmName = $jApp->input->get('frmName');
	$sortName = $jApp->input->get('sortName');
	if ($frmName!=$formId ||!isset($sortName)) $sortName = "Rule.sensorUid";
	$sortOrder = $jApp->input->get('sortOrder');
	if ($frmName!=$formId ||!isset($sortOrder)) $sortOrder = "ASC";

	$user = JFactory::getUser();
	
	$url = JURI::getInstance();
	$currUri = $url->current();
	
	if ($nodeUid==0) return;
	else {
		$filter = "Rule.nodeUid=$nodeUid AND ";
	}
	
	echo "<h2>Rules</h2>";
	echo getEditUrl("/nodes", "nodeUid=$nodeUid&action=addRule", 'Add', '<i class="fa fa-plus"></i>', 'class=headingSideLink');
	echo "<form id=$formId method=get action='$currUri'>";
	echo "<table class=rulelist style='width:100%'>";
	echo "<input type=hidden name=frmName value=$formId />";
	echo "<input type=hidden name=sortName />";
	echo "<input type=hidden name=sortOrder />";
	if ($nodeUid) echo "<input type=hidden name=nodeUid value=$nodeUid />";
	echo "<tr>";
	$fields = getTableHeaderFields('rule');
	$tableColNames = explode('|',$fields[0]);
	$dbColNames = explode('|',$fields[1]);
	for ($i=0; $i<count($tableColNames); $i++) {
		getTableColUrl($formId, $tableColNames[$i], $dbColNames[$i],$sortName,$sortOrder);
	}
	if (!$user->guest)
		echo "<th>Actions</th>";
	echo "</tr>";
	$result = getTableData("SensorInfo,Rule", preg_replace("/\|/",",",$fields[1]), "$filter SensorInfo.sensorUid=Rule.sensorUid ORDER BY $sortName $sortOrder");
	if ($result) {
		foreach ($result as $row) {
			echo "<tr>";
			echo "<td><a href=\"".JURI::base(true)."/index.php/sensors?sensorUid=$row[7]\">$row[0]</a></td>";
			
			echo "<td>$row[1]</td><td>$row[2]</td><td>$row[3]</td><td>$row[4]</td><td>$row[5]</td>";
			
			if (!$user->guest) {
				echo "<td>";
				echo getEditUrl("/nodes","nodeUid=$nodeUid&ruleUid=$row[6]&action=editRule");
				echo getEditUrl("/nodes","nodeUid=$nodeUid&ruleUid=$row[6]&action=delRule",'Delete','&nbsp;&nbsp;<i class="fa fa-trash"></i>');
				echo "</td>";
			}
				
			echo "</tr>";
		}
	}
	else {
		if (!$user->guest) $numCols = 7;
		else $numCols = 6;
		echo "<tr><td class='box-info' style='padding-left:40px' colspan=$numCols>There are no rules attached to this node.</td></tr>";
	}
	echo "</table></form>";
}

function showEvents($nodeUid)
{
	$panId = getPanId();
	$formId = "eventsForm";
	$jApp = JFactory::getApplication();
	$frmName = $jApp->input->get('frmName');
	$sortName = $jApp->input->get('sortName');
	if ($frmName!=$formId ||!isset($sortName)) $sortName = "ts";
	$sortOrder = $jApp->input->get('sortOrder');
	if ($frmName!=$formId ||!isset($sortOrder)) $sortOrder = "DESC";

	$url = JURI::getInstance();
	$currUri = $url->current();
	
	if ($nodeUid==0) { // show events of all nodes
		$filter = "";
	}
	else {
		$filter = "EventInfo.nodeUid=$nodeUid AND ";
	}

	echo "<h2>Events</h2>";
	echo "<form id=$formId method=get action='$currUri'>";
	echo "<table class=eventlist style='width:100%'>";
	echo "<input type=hidden name=frmName value=$formId />";
	echo "<input type=hidden name=sortName />";
	echo "<input type=hidden name=sortOrder />";
	if ($nodeUid) echo "<input type=hidden name=nodeUid value=$nodeUid />";
	echo "<tr>";
	$fields = getTableHeaderFields('eventinfo');
	$tableColNames = explode('|',$fields[0]);
	$dbColNames = explode('|',$fields[1]);
	for ($i=0; $i<count($tableColNames); $i++) {
		if (($nodeUid && $i>1) || $nodeUid==0)
			getTableColUrl($formId, $tableColNames[$i], $dbColNames[$i],$sortName,$sortOrder);
	}
	echo "</tr>";

	if (preg_match("/^(severity|type)$/",$sortName)) $sortStr = "CONCAT($sortName)";
	else $sortStr = $sortName;
	
	$result = getTableData("NodeInfo,EventInfo", preg_replace("/\|/",",",$fields[1]), "$filter NodeInfo.nodeUid=EventInfo.nodeUid AND panId='$panId' ORDER BY $sortStr $sortOrder");
	if ($result) {
		foreach ($result as $row) {
			if ($nodeUid==0) {
				echo "<tr>";
				if ($row[7]!='0001') {
					// show link only if not a coordinator
					echo "<td><a href=\"".preg_replace('/\/events.*/','/nodes',$currUri)."?nodeUid=$row[6]\">$row[0]</a></td>";
				}
				else echo "<td>$row[0]</td>";
				echo "<td>0x$row[1]</td>";
			}
			echo "<td>$row[2]</td><td>$row[3]</td><td>$row[4]</td><td>$row[5]</td></tr>";
		}
	}
	else {
		echo "<tr><td class='box-warning' style='padding-left:40px' colspan=6>There are no events to display.</td></tr>";
	}
	echo "</table></form>";
}

