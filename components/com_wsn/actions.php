<?php

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once(JPATH_COMPONENT.DS.'dbaccess.php');
require_once(JPATH_COMPONENT.DS.'utils.php');

function redirectGuest()
{
	$user = JFactory::getUser();
	if ($user->guest) {
		header("Location: " . JURI::base(true)."/index.php");
		die();
	}
}

function networkConfig()
{
	redirectGuest();
	
	$panId = getPanId();
	$channel = executeQuery("SELECT channel FROM NetworkInfo WHERE panId='$panId'", 0);

	if ($_POST) {
		if ($_POST['channel']) {
			if ($_POST['channel']!=$channel) {
				executeQuery("UPDATE NetworkInfo SET channel='".$_POST['channel']."' WHERE panId='$panId'");
				executeQuery("INSERT INTO PendingAction (type) VALUES ('coord-reset')");
				$channel = $_POST['channel'];
			}
		}
	}

	$actionUrl = $_SERVER['REQUEST_URI'];
	?>
<form method="post" class="customForm" method="POST" action="<?php echo $actionUrl; ?>">
	<div class="formHeader">
		<h3>Network Config</h3>
		<ul>
			<li>This form allows you to configure network-level parameters.</li>
			<li>Configuration is persistent across reboots of the coordinator.</li>
		</ul>
	</div>
	<table>
		<tr>
			<td>
				<label class="fieldDesc" for="channel">Radio Channel</label>
			</td>
			<td>
				<select id="channel" name="channel">
				<?php
					for ($i=1; $i<10; $i++) {
						$currHz = 865199829+199951*$i;
						$currMHz = number_format($currHz/1000000,6);
						if ($channel==$currHz) $selStr = "selected";
						else $selStr = ""; 
						echo "<option value='$currHz' $selStr>$currMHz MHz</option>";
					}
				?>
				</select>
			</td>
		</tr>
		<tr class="buttonRow">
			<td colspan=2>
				<input type="submit" name="submit" value="Save" />
			</td>		
		</tr>
	</table>
</form>
<?php
}

function coordConfig()
{
	redirectGuest();
	
	$panId = getPanId();
	$nodeAssocState = executeQuery("SELECT nodeAssocState FROM NetworkInfo WHERE panId='$panId'", 0);

	if ($_POST) {
		if ($_POST['nodeAssocState']) {
			if ($_POST['nodeAssocState']!=$nodeAssocState) {
				executeQuery("UPDATE NetworkInfo SET nodeAssocState='".$_POST['nodeAssocState']."' WHERE panId='$panId'");
				if ($_POST['nodeAssocState']=='Disabled')
					executeQuery("INSERT INTO PendingAction (type) VALUES ('nj-dis')");
				else
					executeQuery("INSERT INTO PendingAction (type) VALUES ('nj-ena')");
				$nodeAssocState = $_POST['nodeAssocState'];
			}
		}
		
		if ($_POST['addToWhiteList']) {
			$addWL = $_POST['addToWhiteList'];
			$addWL = preg_replace('/[\n]/',',',$addWL);
			$addWL = preg_replace('/[\s:]/','',$addWL);
			$addWL = preg_replace('/,,*/',',',$addWL);
			$addWL = preg_replace('/^,/','',$addWL);
			$addWL = preg_replace('/,$/','',$addWL);
			$addWL = preg_replace('/0[xX]/','',$addWL);
			$addWL = strtoupper($addWL);
			$nodes = explode(',',$addWL);
			$nodes = array_filter($nodes, function($n) { return preg_match('/^[A-F0-9]{16}$/',$n); } );
			$wlNodes = executeQuery("SELECT macAddr FROM NodeWhiteList WHERE panId='$panId'",3);
			$nodes = array_unique(array_diff($nodes,$wlNodes));
			$addWL = implode(',',$nodes);
			$valueStr = implode("','$panId'),('",$nodes) . "','$panId";
			if ($nodes) {
				executeQuery("INSERT IGNORE INTO NodeWhiteList (macAddr, panId) VALUES ('$valueStr')");
				executeQuery("INSERT INTO PendingAction (type, data) VALUES ('wl-add','$addWL')");
			}
		}
		
		if ($_POST['removeFromWhiteList']) {
			$nodes = $_POST['removeFromWhiteList'];
			$removeWL = implode(",",$nodes);
			$valueStr = implode("','",$nodes);
			executeQuery("DELETE FROM NodeWhiteList WHERE macAddr IN ('$valueStr')");
			$numWlNodes = executeQuery("SELECT COUNT(*) FROM NodeWhiteList WHERE panId='$panId'", 0);
			if ($numWlNodes)
				executeQuery("INSERT INTO PendingAction (type, data) VALUES ('wl-del','$removeWL')");
			else
				executeQuery("INSERT INTO PendingAction (type) VALUES ('wl-clr')");
		}
	}
	
	$rows = executeQuery("SELECT DISTINCT(macAddr) FROM NodeWhiteList WHERE panId='$panId'", 3);
	$actionUrl = $_SERVER['REQUEST_URI'];
?>
<form method="post" class="customForm" method="POST" action="<?php echo $actionUrl; ?>">
	<div class="formHeader">
		<h3>Coordinator Config</h3>
		<ul>
			<li>This form allows you to configure coordinator parameters.</li>
			<li>Node association can be globally enabled/disabled.</li>
			<li>An empty white list implies all nodes are allowed by default.</li>
			<li>Configuration is persistent across reboots of the coordinator.</li>
		</ul>
	</div>
	<table>
		<tr>
			<td>
				<label class="fieldDesc" for="nodeAssocState">Node Association</label>
			</td>
			<td>
				<select id="nodeAssocState" name="nodeAssocState">
					<option value="Enabled" <?php if ($nodeAssocState=='Enabled') echo 'selected'; ?>>Enabled</option>
					<option value="Disabled" <?php if ($nodeAssocState=='Disabled') echo 'selected'; ?>>Disabled</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<label class="fieldDesc" for="addToWhiteList">
					Add Node(s) <br>to White List
					<br><span style="font-size:0.8em">(comma separated)</span>
				</label>
			</td>
			<td>
				<textarea type="text" name="addToWhiteList" rows=5></textarea>
			</td>
		</tr>
		<tr>
			<td>
				<label class="fieldDesc" for="removeFromWhiteList[]">
					Remove Node(s) <br>from White List
					<br><span style="font-size:0.8em">(select to remove)</span>
				</label>
			</td>
			<td>
				<select id="nodeAssoc" name="removeFromWhiteList[]" multiple="multiple" size=10>
				<?php
					foreach ($rows as $row) {
						echo "<option value='$row'>$row</option>";
					}
				?>
				</select>
			</td>
		</tr>
		<tr class="buttonRow">
			<td colspan=2>
				<input type="submit" name="submit" value="Save" />
			</td>		
		</tr>
	</table>
</form>
<?php
}

function nodeConfig($nodeUid)
{
	redirectGuest();
	
	$row = getTableData("NodeInfo", "macAddr,macShortAddr,name,location,pushPeriod", "nodeUid=$nodeUid", 1);
	$name = $row[2];
	$location = $row[3];
	$pushPeriod = $row[4];
	$modified = $pushModified = False;
	
	if ($_POST) {
		if (isset($_POST['name']) && $_POST['name']!=='') {
			$name = $_POST['name'];
			$modified = True;
		}

		if (isset($_POST['location']) && $_POST['location']!=='') {
			$location = $_POST['location'];
			$modified = True;
		}
		
		if (isset($_POST['pushPeriod']) && is_numeric($_POST['pushPeriod']) && $pushPeriod!=$_POST['pushPeriod']) {
			$pushPeriod = max(1,min($_POST['pushPeriod'],65535)); // limit to the valid range [1, 65535]
			$pushModified = True;
		}
		
		if ($modified)
			executeQuery("UPDATE NodeInfo SET name='$name', location='$location', pushPeriod=$pushPeriod WHERE nodeUid=$nodeUid");
		
		if ($pushModified)
			executeQuery("INSERT INTO PendingAction (type, data) VALUES ('cfg-dpi','".$row[1]."')");
		
		return;
	}

	$actionUrl = $_SERVER['REQUEST_URI'];
?>
<form method="post" class="customForm" method="POST" action="<?php echo $actionUrl; ?>">
	<div class="formHeader">
		<h3>Node Config</h3>
		<ul>
			<li>This form allows you to configure node-level parameters.</li>
		</ul>
	</div>
	<table>
		<tr>
			<td>
				<label class="fieldDesc" for="macAddr">Node MAC Address</label>
			</td>
			<td>
				<input type="hidden" name="nodeUid" value="<?php echo $nodeUid; ?>" />
				<?php echo $row[0]; ?>
			</td>
		</tr>
		<tr>
			<td>
				<label class="fieldDesc" for="macShortAddr">Node Short Address</label>
			</td>
			<td>
				<?php echo $row[1]; ?>
			</td>
		</tr>
		<tr>
			<td>
				<label class="fieldDesc" for="name">
					Node Name
				</label>
			</td>
			<td>
				<input type="text" name="name" value="<?php echo $name; ?>" />
			</td>
		</tr>
		<tr>
			<td>
				<label class="fieldDesc" for="location">
					Location
				</label>
			</td>
			<td>
				<input type="text" name="location" value="<?php echo $location; ?>" />
			</td>
		</tr>
		<tr>
			<td>
				<label class="fieldDesc" for="pushPeriod">
					Periodic Reporting Interval (secs)
				</label>
			</td>
			<td>
				<input type="text" name="pushPeriod" value="<?php echo $pushPeriod; ?>" />
			</td>
		</tr>
		<tr class="buttonRow">
			<td colspan=2>
				<input type="submit" name="submit" value="Save" />
			</td>		
		</tr>
	</table>
</form>
<?php
}

function nodeDelete($nodeUid=0)
{
	redirectGuest();

	$queries = array();
	if ($nodeUid) {
		list($panId,$macAddr) = getTableData("NodeInfo", "panId,macAddr", "nodeUid=$nodeUid", 3);
		$queries[] = "DELETE FROM NodeWhiteList WHERE panId='$panId' AND macAddr='$macAddr'";
		$queries[] = "DELETE FROM RouteInfo WHERE nodeUid=$nodeUid";
		$queries[] = "DELETE FROM SensorData WHERE sensorUid IN (SELECT sensorUid FROM SensorInfo WHERE nodeUid=$nodeUid)";
		$queries[] = "DELETE FROM SensorInfo WHERE nodeUid=$nodeUid";
		$queries[] = "DELETE FROM EventInfo WHERE nodeUid=$nodeUid";
		$queries[] = "DELETE FROM Rule WHERE nodeUid=$nodeUid";
		$queries[] = "DELETE FROM NodeInfo WHERE nodeUid=$nodeUid";
		$queries[] = "UPDATE NetworkInfo SET nodeCount=nodeCount-1 WHERE panId='$panId'";
	}
	else { // delete all nodes of current PAN except the PAN coordinator
		$panId = getPanId();
		$nodeUids = implode(',',getTableData("NodeInfo", "nodeUid", "panId='$panId' AND macShortAddr!='0001'", 3));
		$queries[] = "DELETE FROM NodeWhiteList WHERE panId='$panId'";
		$queries[] = "DELETE FROM RouteInfo WHERE nodeUid IN ($nodeUids)";
		$queries[] = "DELETE FROM SensorData WHERE sensorUid IN (SELECT sensorUid FROM SensorInfo WHERE nodeUid IN ($nodeUids))";
		$queries[] = "DELETE FROM SensorInfo WHERE nodeUid IN ($nodeUids)";
		$queries[] = "DELETE FROM EventInfo WHERE nodeUid IN ($nodeUids)";
		$queries[] = "DELETE FROM Rule WHERE nodeUid IN ($nodeUids)";
		$queries[] = "DELETE FROM NodeInfo WHERE nodeUid IN ($nodeUids)";
	}
	

	executeTransaction($queries);
}

function digitalIOControl($nodeUid)
{
	redirectGuest();
	
	if ($_POST) {
		$macShortAddr = getTableData("NodeInfo", "macShortAddr", "nodeUid=$nodeUid", 0);
		$data = "$macShortAddr,".$_POST['portNum'].",".$_POST['pinNum'].",".$_POST['gpioValue'];
		executeQuery("INSERT INTO PendingAction (type, data) VALUES ('dioc','$data')");
		return;
	}
	
	$actionUrl = JURI::base(true)."/index.php/nodes?nodeUid=$nodeUid&action=digitalIOCtrl";
?>
<form method="post" class="customForm" method="POST" action="<?php echo $actionUrl; ?>">
	<table class="digitalIOControl" style="width:100%">
		<tr>
			<th>
				Digital IO
			</th>
			<td>
				<input type="hidden" name="nodeUid" value="<?php echo $nodeUid; ?>" />
				<select id="portNum" name="portNum">
				<?php
					for ($i=1; $i<=4; $i++) {
						echo "<option value='$i'>Port $i</option>";
					}
				?>
				</select>
			</td>
			<td>
				<select id="pinNum" name="pinNum">
				<?php
					for ($i=0; $i<8; $i++) {
						echo "<option value='$i'>Pin $i</option>";
					}
				?>
				</select>
			</td>
			<td>
				<select id="gpioValue" name="gpioValue">
					<option value='0'>HIGH</option>
					<option value='1'>LOW</option>
				</select>
			</td>
			<td>
				<input type="submit" name="submit" value="Apply" style="float:right" />
			</td>		
		</tr>
	</table>
</form>
<?php	
}

function editRule($nodeUid, $ruleUid)
{
	redirectGuest();
	
	$node = getTableData("NodeInfo", "macAddr,macShortAddr", "nodeUid=$nodeUid", 1);
	$sensors = getTableData("SensorInfo", "sensorUid,manufacturer,partNum,type", "nodeUid=$nodeUid");
	$types = getEnumValue('Rule','type');
	$actionPoints = getEnumValue('Rule','actionOn');
	if ($ruleUid) {
		$action = 'Edit';
		$rule = getTableData("Rule", "sensorUid,type,threshold,notifyPeriod,mobileNum,actionOn", "ruleUid=$ruleUid", 1);
		$sensorUid = $rule[0];
		$type = $rule[1];
		$threshold = $rule[2];
		$notifyPeriod = $rule[3];
		$mobileNum = $rule[4];
		$actionOn = $rule[5];
	}
	else {
		$action = 'Add';
		$type = $types[0];
		$actionOn = $actionPoints[0];
		$threshold = $notifyPeriod = $mobileNum = '';
	}

	if ($_POST) {
		if (is_numeric($_POST['threshold']) &&
			is_numeric($_POST['notifyPeriod'])) {
			$sensorUid = $_POST['sensorUid'];
			$type = $_POST['type'];
			$threshold = $_POST['threshold'];
			$notifyPeriod = max(1,$_POST['notifyPeriod']);
			$mobileNum = $_POST['mobileNum'];
			$actionOn = $_POST['actionOn'];
			if (!preg_match('/^\d{10}$/',$mobileNum)) $mobileNum = '';
			if ($ruleUid)
				executeQuery("UPDATE Rule SET sensorUid=$sensorUid, type='$type', threshold=$threshold, actionOn='$actionOn', notifyPeriod=$notifyPeriod, mobileNum='$mobileNum' WHERE ruleUid=$ruleUid");
			else
				executeQuery("INSERT INTO Rule (nodeUid,sensorUid,type,threshold,actionOn,notifyPeriod,mobileNum) VALUES($nodeUid,$sensorUid,'$type',$threshold,'$actionOn',$notifyPeriod,'$mobileNum')");
		}
		return;
	}

	$actionUrl = $_SERVER['REQUEST_URI'];
?>
<form method="post" class="customForm" method="POST" action="<?php echo $actionUrl; ?>">
	<div class="formHeader">
		<h3><?php echo $action; ?> Rule</h3>
		<ul>
			<li>This form allows you to configure a rule based on sensor data.</li>
			<li>Rule is persistent across node reassociations.</li>
		</ul>
	</div>
	<table>
		<tr>
			<td>
				<label class="fieldDesc" for="macAddr">Node MAC Address</label>
			</td>
			<td>
				<input type="hidden" name="nodeUid" value="<?php echo $nodeUid; ?>" />
				<input type="hidden" name="ruleUid" value="<?php echo $ruleUid; ?>" />
				<?php echo $node[0]; ?>
			</td>
		</tr>
		<tr>
			<td>
				<label class="fieldDesc" for="macShortAddr">Node Short Address</label>
			</td>
			<td>
				<?php echo $node[1]; ?>
			</td>
		</tr>
		<tr>
			<td>
				<label class="fieldDesc" for="sensorUid">
					Sensor
				</label>
			</td>
			<td>
				<select id="sensorUid" name="sensorUid">
				<?php
					foreach ($sensors as $sensor) {
						if ($sensor[0]==$sensorUid) $selStr = 'selected="selected"';
						else $selStr = '';
						echo "<option value='$sensor[0]' $selStr>";
						echo "$sensor[1] / $sensor[2] / $sensor[3]";
						echo "</option>";
					}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<label class="fieldDesc" for="type">
					Rule Type
				</label>
			</td>
			<td>
				<select id="type" name="type">
				<?php
					foreach ($types as $t) {
						if ($t==$type) $selStr = 'selected="selected"';
						else $selStr = '';
						echo "<option value='$t' $selStr>$t</option>";
					}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<label class="fieldDesc" for="threshold">
					Threshold
				</label>
			</td>
			<td>
				<input type="text" name="threshold" value="<?php echo $threshold; ?>" />
			</td>
		</tr>
		<tr>
			<td>
				<label class="fieldDesc" for="actionOn">
					Action On
				</label>
			</td>
			<td>
				<select id="type" name="actionOn">
				<?php
					foreach ($actionPoints as $ap) {
						if ($ap==$actionOn) $selStr = 'selected="selected"';
						else $selStr = '';
						echo "<option value='$ap' $selStr>$ap</option>";
					}
				?>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				<label class="fieldDesc" for="notifyPeriod">
					Notify Period
					<br><span style="font-size:0.8em">(secs)</span>
				</label>
			</td>
			<td>
				<input type="text" name="notifyPeriod" value="<?php echo $notifyPeriod; ?>" />
			</td>
		</tr>
		<tr>
			<td>
				<label class="fieldDesc" for="mobileNum">
					Mobile Number
					<br><span style="font-size:0.8em">(for SMS notification)</span>
				</label>
			</td>
			<td>
				<input type="text" name="mobileNum" value="<?php echo $mobileNum; ?>" />
			</td>
		</tr>
		<tr class="buttonRow">
			<td colspan=2>
				<input type="submit" name="submit" value="Save" />
			</td>		
		</tr>
	</table>
</form>
<?php
}

function deleteRule($nodeUid, $ruleUid)
{
	redirectGuest();
	
	executeQuery("DELETE FROM Rule WHERE ruleUid=$ruleUid");
}

