<?php

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once(JPATH_COMPONENT.DS.'dbaccess.php');

function getOptStr($opts, $selVal='')
{	// $opts: array of array(value,name)
	$str = '';
	foreach ($opts as $opt) {
		if (!is_array($opt)) $opt = array($opt, $opt);
		if ($selVal!='' && $selVal==$opt[0])
			$str .= "<option selected=selected value='$opt[0]'>$opt[1]</option>";
		else $str .= "<option value='$opt[0]'>$opt[1]</option>";
	}
	return $str;
}

function getUserPanIds()
{
	$user = JFactory::getUser();
	$panIds = getTableData("#__usergroups,#__user_usergroup_map", "title",
			"#__user_usergroup_map.user_id=".$user->id
			." AND #__user_usergroup_map.group_id=#__usergroups.id"
			." AND title!='Registered'", 3);
	return $panIds;
}

function setPanCookie($panIds)
{
	$filterPanId = JRequest::getCmd('panFilter');
	$cookiePanId = getPanCookie();
	if ($filterPanId && $cookiePanId!=$filterPanId) {
		deletePanCookie();
		setcookie('panId', $filterPanId, 0, "/");
		$_COOKIE['panId'] = $filterPanId;
		return $filterPanId;
	}
	else if ($cookiePanId===null) {
		setcookie('panId', $panIds[0], 0, "/");
		$_COOKIE['panId'] = $panIds[0];
		return $panIds[0];
	}
	else {
		return $cookiePanId;
	}
}

function getPanCookie()
{
	if (isset($_COOKIE['panId']))
		return $_COOKIE['panId'];
	else
		return null;
}

function deletePanCookie()
{
	if (isset($_COOKIE['panId']))
		unset($_COOKIE['panId']);
}

function getPanId()
{
	$panId = getPanCookie();
	if ($panId===null) {
		$panIds = getUserPanIds();
		$panId = $panIds[0];
	}
	return $panId;
}

function getEditUrl($path, $queryStr, $title='Edit', $display='<i class="fa fa-edit"></i>', $styleStr='')
{
	$user = JFactory::getUser();
	if ($user->guest) return "";

	$baseUrl = JURI::base(true).'/index.php';
	return "<a href='$baseUrl$path?$queryStr' $styleStr title='$title'>$display</a>";
}

