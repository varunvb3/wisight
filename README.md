# Overview #

*WiSight* is an open-source software that enables network management and monitoring for IoT applications. Data is typically coming from sensors in the network. *WiSight* stores this data in a database. Basic data visualization is possible. Software is implemented as a web application.
![wisight-screenshot.png](https://bitbucket.org/repo/KX65nb/images/1155243306-wisight-screenshot.png)

# Installation #
## Ubuntu ##
* On Ubuntu, a web server is available by default at `/var/www`.
* [Download WiSight](https://bitbucket.org/arvindpdmn/wisight/downloads) as a zip file and unpack the contents into `/var/www/html/wisight`. For the rest of this guide, files and folders are w.r.t. `/var/www/html/wisight`.
* Configure access to MySQL DB by editing these lines in file `configuration.php` (change `root` to username applicable to your MySQL server and supply the password):
```
	public $user = 'root';
	public $password = '';
```
* Import DB by running the following command (change `root` to username applicable to your MySQL server):
```
	mysql -u root < wisight.sql
```
* Grant write permissions to all users to specific folders as noted below:
```
	chmod -R a+w ./ cache/ tmp/ logs/
```
* That's it! You may now access *WiSight* from any web browser by entering the URL `http://localhost/wisight`. To access the application by any other name of your choice, you may rename `wisight` sub-folder.

## Windows ##
* To run this software, a web server must be running. [WampServer](www.wampserver.com/en/) is recommended as the web server. Install this first. For the rest of this guide, we assume that WampServer is installed at `c:\wamp`
* [Download WiSight](https://bitbucket.org/arvindpdmn/wisight/downloads) as a zip file and unpack the contents into `c:\wamp\www\wisight`. For the rest of this guide, files and folders are w.r.t. `c:\wamp\www\wisight`.
* Edit the file `configuration.php` as follows (change `root` to username applicable to your MySQL server and supply the password):
```
	public $user = 'root';
	public $password = '';
	public $log_path = 'c:\\wamp\\www\\wisight\\logs';
	public $log_path = 'c:\\wamp\\www\\wisight\\tmp';
```
* Import DB from file `wisight.sql` into your MySQL.
* Grant write permissions to all users to specific folders noted below:
```
.
cache
tmp
logs
```
* That's it! You may now access *WiSight* from any web browser by entering the URL as `http://localhost/wisight`. To access the application by any other name of your choice, you may rename `wisight` sub-folder.

# Joomla! Admin Password #
* Administrator can access the Joomla! administrator interface at `http://localhost/wisight/administrator` with username `admin` and password `wiSi_2015`.

# Developer Notes #
* Database is named `wisight`. A backup of the database is kept at `wisight.sql`.
* Basic Joomla! installation has been customized by installing a component named `com_wsn`. The bulk of the implementation has been coded in `components/com_wsn/wsn.php` and `components/com_wsn/actions.php`. The latter file allows display of forms so that user can configure the LPWMN and send triggers.
* It is possible to generate test data by setting `$Simulation = 1` in file `components/com_wsn/wsn.php`. Actual generation is done in `components/com_wsn/simulator.php`.
* In Joomla! administrator interface, you may be prompted to upgrade Joomla! installation. Do not perform this upgrade since this will overwrite all code changes done on top of Joomla!.
* Current implementation allows management of multiple LPWMN networks. User logins can be given access to specific networks. This is configured in the User Manager of Joomla! administrator panel. Each network is identified by a 4-digit (in hexadecimal) LPWMN ID, which must appears as a subgroup of the Registered group.
* Note that login form appears on the main menu as well as on the side panel. This is because the former is not visible when viewing on small devices such as smartphones.

# Limitations (1-Apr-2015) #
* Pagination is not done on any of the lists.
* Display of graph is static. AJAX can be employed to update display without requiring users to reload the page.
* Big data analytics to take automated actions based on sensor inputs are not part of current implementation. Essentially, current implementation is mainly for demo and training. In its current form, it is not meant for commercial deployment. However, a basic interface to configure simple rules is provided.

# Technologies #
* Joomla! is used as the content management system (CMS) for this web application. Version used is 2.5.13.
* Joomla! template used is `Warp Master Theme` from [YOOThemes](https://yootheme.com/).
* Server-side programming is in PHP. Database used in MySQL. App has been tested on Apache server.
* Data visualization is done using [d3.js](http://d3js.org/).

# Licensing #
* Please refer to the file named `UNLICENSE` for details.
